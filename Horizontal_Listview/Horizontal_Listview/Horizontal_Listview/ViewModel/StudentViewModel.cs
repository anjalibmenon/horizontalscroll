﻿using Horizontal_Listview.Model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;

namespace Horizontal_Listview.ViewModel
{
   public class StudentViewModel
    {
        public ObservableCollection<Student> student_obs { get; set; } = new ObservableCollection<Student>();

        public StudentViewModel()
        {
            GetAllData();
        }

        private void GetAllData()
        {
            student_obs.Add(new Student("Nidhin", "9746759321", "ic_user.png"));
            student_obs.Add(new Student("Anjali", "9656825393", "ic_user.png"));
            student_obs.Add(new Student("Habi", "9995760037", "ic_user.png"));
            student_obs.Add(new Student("Lachu", "9746759321", "ic_user.png"));
            student_obs.Add(new Student("Jessy", "9656825393", "ic_user.png"));
            student_obs.Add(new Student("Swa", "9995760037", "ic_user.png"));
        }

    }
}
