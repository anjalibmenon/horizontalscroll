﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Horizontal_Listview.Model
{
   public class Student
    {
        public Student(string name, string mobile, string image)
        {
            Name = name;
            Mobile = mobile;
            Image = image;
        }

        public string Name { get; set; }
        public string Mobile { get; set; }
        public string Image { get; set; }

    }
}
